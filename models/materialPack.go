package models

import (
	"errors"
	"fmt"
)

var (
	MockMaterialPack     MaterialPack
	MockMaterialPackList []MaterialPack
)

func init() {

	for i := 1; i <= 5; i++ {
		MockMaterialPackList = append(MockMaterialPackList, MaterialPack{
			fmt.Sprintf("507f1f77bcf86cd79943901%d", i),
			fmt.Sprintf("Title %d.", i),
			fmt.Sprintf("Content We are currently processing ticket number %d.", i),
			"",
			"",
			"",
			"",
			[]string{"cat", "doc"},
			[]string{"507f1f77bcf86cd799439011", "507f1f77bcf86cd799439012"},
			[]string{"507f1f77bcf86cd799439011", "507f1f77bcf86cd799439012"},
		})
	}
	MockMaterialPack = MockMaterialPackList[0]

}

type MaterialPack struct {
	ID          string   `json:"id"`
	Title       string   `json:"title"`
	Content     string   `json:"content"`
	CreatedAt   string   `json:"created_at"`
	CreatedBy   string   `json:"created_by"`
	UpdatedAt   string   `json:"updated_at"`
	UpdatedBy   string   `json:"updated_by"`
	Tags        []string `json:"tags"`
	CategoryIds []string `json:"category_ids"`
	MaterialIds []string `json:"material_ids"`
}

type CommonResponse struct {
	StatusCode    int    `json:"status_code"`
	StatusMessage string `json:"status_message"`
}

func GetAllMaterialPack() []MaterialPack {
	return MockMaterialPackList
}
func GetMaterialPack(id string) (m MaterialPack, err error) {
	var model MaterialPack
	for i := 0; i < len(MockMaterialPackList); i++ {
		if id == MockMaterialPackList[i].ID {
			model = MockMaterialPackList[i]
		}
	}
	return model, nil

}

func UpdateMaterialPack(id string, c *MaterialPack) (o *MaterialPack, err error) {
	if id != "" {
		return c, nil
	}
	return nil, errors.New("MaterialPack Not Exist")
}
func DeleteMaterialPack(id string) (result int) {
	return 1
}
