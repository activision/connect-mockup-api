package models

type SearchMaterialPackResponse struct {
	Page        int            `json:"page"`
	Results     []MaterialPack `json:"results"`
	TotalResult int            `json:"total_result"`
	TotalPage   int            `json:"total_page"`
}
type SearchFailureResponse struct {
	StatusCode    int    `json:"status_code"`
	StatusMessage string `json:"status_message"`
}

func SearchMaterialPack() SearchMaterialPackResponse {
	var results = MockMaterialPackList
	response := SearchMaterialPackResponse{
		1,
		results,
		len(results),
		2,
	}
	return response
}
