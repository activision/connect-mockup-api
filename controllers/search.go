package controllers

import (
	"connect-api/models"

	"github.com/astaxie/beego"
)

// About Search in project
type SearchController struct {
	beego.Controller
}

// @Title MaterialPackSearch
// @Param	q		query	string		"query"
// @Param	page	query	number		"page"
// @Param	category_id	 query	string		"category"
// @Param	province_id	 query	string		"province"
// @Param	region_id	 query	string		"region"
// @Param	contributor_id	 query	string		"contributor"
// @Param	start_date	 query	string		"start date"
// @Param	end_date	 query	string		"end date"
// @Description Search material pack
// @Success 200 {object} models.SearchMaterialPackResponse
// @Failure 404	not found
// @router /material-pack [get]
func (c *SearchController) MaterialPack() {
	results := models.SearchMaterialPack()
	c.Data["json"] = results
	c.ServeJSON()
}
