package controllers

import (
	"connect-api/models"

	"github.com/astaxie/beego"
)

// Material Pack
type MaterialPackController struct {
	beego.Controller
}

// @Title Get
// @Description get material pack by id
// @Param	id		path 	string	true		"id"
// @Success 200 {object} models.MaterialPack
// @Failure 403 :id is empty
// @router /:id [get]
func (c *MaterialPackController) Get() {
	id := c.GetString(":id")
	if id != "" {
		model, err := models.GetMaterialPack(id)
		if err != nil {
			c.Data["json"] = err.Error()
		} else {
			c.Data["json"] = model
		}
	}
	c.ServeJSON()
}

// @Title Delete
// @Description delete the material pack by id
// @Param	id		path 	string	true		"id"
// @Success 200 {string} delete success!
// @Failure 403 id is empty
// @router /delete/:id [delete]
func (c *MaterialPackController) Delete() {
	id := c.GetString(":id")
	models.DeleteMaterialPack(id)
	c.Data["json"] = "delete success!"
	c.ServeJSON()
}

// @Title Create
// @Description create material pack
// @Param	params	body	models.MaterialPack	true		"params for create material pack"
// @Success 200 {string} create success!
// @Failure
// @router /create [post]
func (c *MaterialPackController) Create() {
	c.Data["json"] = "create success!"
	c.ServeJSON()
}

// @Title Update
// @Description update material pack
// @Param	id		path 	string	true		"id"
// @Param	params	body	models.MaterialPack	true		"params for update material pack"
// @Success 200 {object} models.CommonResponse
// @Failure
// @router /update/:id [put]
func (c *MaterialPackController) Put() {

	c.Data["json"] = models.CommonResponse{
		200,
		"create success!",
	}
	c.ServeJSON()
}
