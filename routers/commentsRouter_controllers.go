package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["connect-api/controllers:MaterialPackController"] = append(beego.GlobalControllerRouter["connect-api/controllers:MaterialPackController"],
        beego.ControllerComments{
            Method: "Get",
            Router: `/:id`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["connect-api/controllers:MaterialPackController"] = append(beego.GlobalControllerRouter["connect-api/controllers:MaterialPackController"],
        beego.ControllerComments{
            Method: "Create",
            Router: `/create`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["connect-api/controllers:MaterialPackController"] = append(beego.GlobalControllerRouter["connect-api/controllers:MaterialPackController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: `/delete/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["connect-api/controllers:MaterialPackController"] = append(beego.GlobalControllerRouter["connect-api/controllers:MaterialPackController"],
        beego.ControllerComments{
            Method: "Put",
            Router: `/update/:id`,
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["connect-api/controllers:SearchController"] = append(beego.GlobalControllerRouter["connect-api/controllers:SearchController"],
        beego.ControllerComments{
            Method: "MaterialPack",
            Router: `/material-pack`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["connect-api/controllers:UserController"] = append(beego.GlobalControllerRouter["connect-api/controllers:UserController"],
        beego.ControllerComments{
            Method: "Post",
            Router: `/`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["connect-api/controllers:UserController"] = append(beego.GlobalControllerRouter["connect-api/controllers:UserController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: `/`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["connect-api/controllers:UserController"] = append(beego.GlobalControllerRouter["connect-api/controllers:UserController"],
        beego.ControllerComments{
            Method: "Get",
            Router: `/:uid`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["connect-api/controllers:UserController"] = append(beego.GlobalControllerRouter["connect-api/controllers:UserController"],
        beego.ControllerComments{
            Method: "Put",
            Router: `/:uid`,
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["connect-api/controllers:UserController"] = append(beego.GlobalControllerRouter["connect-api/controllers:UserController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: `/:uid`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["connect-api/controllers:UserController"] = append(beego.GlobalControllerRouter["connect-api/controllers:UserController"],
        beego.ControllerComments{
            Method: "Login",
            Router: `/login`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["connect-api/controllers:UserController"] = append(beego.GlobalControllerRouter["connect-api/controllers:UserController"],
        beego.ControllerComments{
            Method: "Logout",
            Router: `/logout`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
